Source: aiomysql
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Adam Cecile <acecile@le-vert.net>
Build-Depends: debhelper-compat (= 12),
               pybuild-plugin-pyproject,
               python3-all,
               python3-setuptools,
               python3-pymysql,
               python3-setuptools-scm,
               python3-sphinx <!nodoc>,
               python3-sphinxcontrib-asyncio <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
# Integration tests, using local MySQL and docker
# Build locally with DEB_BUILD_PROFILES="pkg.aiomysql.integrationtests"
# See debian/README.integration_tests to setup your local machine
              python3-pytest <pkg.aiomysql.integrationtests>,
              python3-docker <pkg.aiomysql.integrationtests>,
              python3-pymysql (>= 0.9~) <pkg.aiomysql.integrationtests>,
              python3-sqlalchemy (>= 1.0~) <pkg.aiomysql.integrationtests>,
              python3-uvloop <pkg.aiomysql.integrationtests>,
Standards-Version: 4.4.1
Homepage: https://github.com/aio-libs/aiomysql
Vcs-Git: https://salsa.debian.org/python-team/packages/aiomysql.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/aiomysql

Package: python3-aiomysql
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Recommends: python3-sqlalchemy (>= 1.0~)
Suggests: python-aiomysql-doc
Description: library for accessing a MySQL using asyncio (Python 3)
 aiomysql is a driver for accessing a MySQL database from the asyncio
 framework.
 .
 It depends on and reuses most parts of PyMySQL.
 .
 aiomysql tries to be like awesome aiopg library and preserve same API,
 look and feel.
 .
 This package installs the library for Python 3.

Package: python-aiomysql-doc
Build-Profiles: <!nodoc>
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: library for accessing a MySQL database from the asyncio (common documentation)
 aiomysql is a driver for accessing a MySQL database from the asyncio
 framework.
 .
 It depends on and reuses most parts of PyMySQL.
 .
 aiomysql tries to be like awesome aiopg library and preserve same API,
 look and feel.
 .
 This is the common documentation package.
